<?php
ini_set('display_errors', 'On');
set_time_limit(900);
require('consultas.php');
date_default_timezone_set('America/Santiago');

if(count($_GET) >= 0){
	$fecha = date('Y-m-d');
	$nuevafecha = strtotime ( '-0 day' , strtotime ( $fecha ) ) ;
	$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
  $fecha_carga= $nuevafecha;
	$hora_carga=date("H").":".date("i");
  $usuario = 'mmrojas';
  $pass = 'Mv031721';

	$chr_map = array(
		 // Windows codepage 1252
		 "\xC2\x82" => "'", // U+0082⇒U+201A single low-9 quotation mark
		 "\xC2\x84" => '"', // U+0084⇒U+201E double low-9 quotation mark
		 "\xC2\x8B" => "'", // U+008B⇒U+2039 single left-pointing angle quotation mark
		 "\xC2\x91" => "'", // U+0091⇒U+2018 left single quotation mark
		 "\xC2\x92" => "'", // U+0092⇒U+2019 right single quotation mark
		 "\xC2\x93" => '"', // U+0093⇒U+201C left double quotation mark
		 "\xC2\x94" => '"', // U+0094⇒U+201D right double quotation mark
		 "\xC2\x9B" => "'", // U+009B⇒U+203A single right-pointing angle quotation mark

		 // Regular Unicode     // U+0022 quotation mark (")
														// U+0027 apostrophe     (')
		 "\xC2\xAB"     => '"', // U+00AB left-pointing double angle quotation mark
		 "\xC2\xBB"     => '"', // U+00BB right-pointing double angle quotation mark
		 "\xE2\x80\x98" => "'", // U+2018 left single quotation mark
		 "\xE2\x80\x99" => "'", // U+2019 right single quotation mark
		 "\xE2\x80\x9A" => "'", // U+201A single low-9 quotation mark
		 "\xE2\x80\x9B" => "'", // U+201B single high-reversed-9 quotation mark
		 "\xE2\x80\x9C" => '"', // U+201C left double quotation mark
		 "\xE2\x80\x9D" => '"', // U+201D right double quotation mark
		 "\xE2\x80\x9E" => '"', // U+201E double low-9 quotation mark
		 "\xE2\x80\x9F" => '"', // U+201F double high-reversed-9 quotation mark
		 "\xE2\x80\xB9" => "'", // U+2039 single left-pointing angle quotation mark
		 "\xE2\x80\xBA" => "'", // U+203A single right-pointing angle quotation mark
	);
	$chr = array_keys  ($chr_map); // but: for efficiency you should
	$rpl = array_values($chr_map); // pre-calculate these two arrays

	//Lo primerito, creamos una variable iniciando curl, pasándole la url
	$ch = curl_init('https://www.despachonacional.cl/siad_reportes/controlador/init_session.php');
	$cookie = "descarga/cookieSI.txt";

	//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
	curl_setopt ($ch, CURLOPT_POST, 1);

	//le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
	curl_setopt ($ch, CURLOPT_POSTFIELDS, "user=" . $usuario .  "&pass=" . $pass);

	//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

	//recogemos la respuesta
	$respuesta = curl_exec($ch);

  curl_close($ch);

	//DTH Notif
	$ch4 = curl_init();
	$fp3 = fopen("descarga/notif_dth.xls", "w+");

	$url = "https://www.despachonacional.cl/siad_reportes/controlador/export_inf_despacho_nacional_.php";

	curl_setopt($ch4, CURLOPT_URL, $url);
	curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
	curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch4, CURLOPT_FILE, $fp3);
	$content4 = curl_exec($ch4);
	fwrite($fp3, $content4);
	fclose($fp3);

  curl_close($ch4);

	//HFC Notif
	$ch5 = curl_init();
	$fp4 = fopen("descarga/notif_hfc.xls", "w+");

	$url = "https://www.despachonacional.cl/siad_reportes/controlador/export_hfc.php";

	curl_setopt($ch5, CURLOPT_URL, $url);
	curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
	curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch5, CURLOPT_FILE, $fp4);
	$content5 = curl_exec($ch5);
	fwrite($fp4, $content5);
	fclose($fp4);

  curl_close($ch5);

  //PYME Notif
	$ch6 = curl_init();
	$fp5 = fopen("descarga/notif_pyme.xls", "w+");

	$url = "https://www.despachonacional.cl/siad_reportes/controlador/export_pyme.php";

	curl_setopt($ch6, CURLOPT_URL, $url);
	curl_setopt($ch6, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
	curl_setopt($ch6, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch6, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch6, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch6, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch6, CURLOPT_FILE, $fp5);
	$content6 = curl_exec($ch6);
	fwrite($fp5, $content6);
	fclose($fp5);

  curl_close($ch6);

	//Carga de datos DTH
	$archivo="descarga/notif_dth.xls";

	$data = file_get_contents('descarga/notif_dth.xls');

	$array_data = explode("<tr >", $data);

	for ($i = 1; $i < count($array_data); $i++)
	{
		$fila = explode("<td>", str_replace("bgcolor='#F2F2F2'>","",str_replace("bgcolor='#EFF8FB'>","",$output = iconv('UTF-8', 'ASCII//TRANSLIT', $array_data[$i]))));
 	 $fila = str_replace($chr, $rpl, $fila);
  	for($j = 0; $j < count($fila); $j++){
  		$fila[$j] = trim(str_replace("'", "", str_replace("</tr>", "", str_replace("</td>", "", $fila[$j]))));
  	}

 		$carga = insertDatosClaroDTH($fila[1],$fila[2],$fila[3],$fila[4],$fila[5],$fila[6],$fila[7],$fila[8],$fila[9],$fila[10],$fila[11],$fila[12],$fila[13],$fila[14],$fila[15],$fila[16],$fila[17],$fila[18],$fila[19],$fila[20],$fila[21],$fila[22],$fila[23],$fila[24],$fila[25],$fila[26],$fila[27],$fila[28],$fila[29],$fila[30],$fila[31],$fila[32],$fila[33],$fila[34],$fila[39],$fila[40],$fila[35],$fila[36],$fila[37],$fila[38],$fecha_carga,$hora_carga);

    echo $carga . "<br>";
	}

	//Carga de datos HFC
	$archivo="descarga/notif_hfc.xls";

	$data = file_get_contents('descarga/notif_hfc.xls');

	$array_data = explode("<tr", $data);

	for ($i = 2; $i < count($array_data); $i++)
	{
    $fila = explode("<td>", str_replace("bgcolor='#F2F2F2'>","",str_replace("bgcolor='#EFF8FB'>","", utf8_encode($array_data[$i]))));
		$fila = str_replace($chr, $rpl, $fila);
  	for($j = 0; $j < count($fila); $j++){
  		$fila[$j] = trim(str_replace("'", "", str_replace("</tr>", "", str_replace("</td>", "", $fila[$j]))));
		}

		$carga = insertDatosClaroHFC($fila[1],$fila[2],$fila[3],$fila[4],$fila[5],$fila[6],$fila[7],$fila[8],$fila[9],$fila[10],$fila[11],$fila[12],$fila[13],$fila[14],$fila[15],$fila[16],$fila[17],$fila[18],$fila[19],$fila[20],$fila[21],$fila[22],$fila[23],$fila[24],$fila[25],$fila[26],$fila[27],$fila[28],$fila[29],$fila[30],$fila[31],$fila[32],$fila[33],$fila[34],$fila[35],$fila[36],$fila[37],$fila[38],$fila[39],$fila[40],$fila[41],$fila[42],$fila[43],$fila[44],$fila[45],$fila[46],$fila[47],$fila[48],$fila[49],$fila[50],$fila[51],$fecha_carga,$hora_carga);

		echo $carga . "<br>";
	}


	echo normalizaDTH();
	echo normalizaDTHProgramacion();
	echo normalizaDTHAgendamiento();
	echo cargaOTDTH();

	// unlink('/home/rriveros/public_html/get_claro_siad/descarga/notif_pyme.xls');
	// unlink('/home/rriveros/public_html/get_claro_siad/descarga/notif_hfc.xls');
	// unlink('/home/rriveros/public_html/get_claro_siad/descarga/notif_dth.xls');
}
?>
