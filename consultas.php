<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

  function insertDatosClaroDTH($A,$B,$C,$D,$E,$F,$G,$H,$I,$J,$K,$L,$M,$N,$O,$P,$Q,$R,$S,$T,$U,$V,$W,$X,$Y,$Z,$AA,$AB,$AC,$AD,$AE,$AF,$AG,$AH,$AI,$AJ,$AK,$AL,$AM,$AN,$fecha_carga, $hora_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_DTH
(TIPO, DISTRIBUIDOR, VENDEDOR, TKFECHA, TKHORA, TIEMPO, DH, NATENCION, DECOS, PLANDESCRIPCION, COSTO_INSTALACION, FECHA_AGENDA, HORA_AGENDA, CANTIDAD_REAGENDAMIENTOS, RUT, CLIENTE, REGION, COMUNA, CALLE, NUMERO, DEPTO, FONO1, FONO2, REFERENCIA, TIPO_CALLE, TIPO_TICKET, NOMBRE_ZONA, OBSERVACIONES, `kit tarjetas`, `kit lnb`, `kit splitter`, `kit antena`, `kit cable`, `kit brazo telescopico`, `NOMBRE ALIADO`, `ESTADO ORDEN`, QTY_DECO_SD, QTY_DECO_HD, QTY_DECO_PVR, QTY_DECO_TOTAL, fecha_carga, hora_carga, ESTADO_DESPACHO)
VALUES('{$A}','{$B}','{$C}','{$D}','{$E}','{$F}','{$G}','{$H}','{$I}','{$J}','{$K}','{$L}','{$M}','{$N}','{$O}','{$P}','{$Q}','{$R}','{$S}','{$T}','{$U}','{$V}','{$W}','{$X}','{$Y}','{$Z}','{$AA}','{$AB}','{$AC}','{$AD}','{$AE}','{$AF}','{$AG}','{$AH}','{$AI}','{$AJ}','{$AK}','{$AL}','{$AM}','{$AN}','{$fecha_carga}','{$hora_carga}','Programacion')
ON DUPLICATE KEY UPDATE
`ESTADO ORDEN` = '{$AJ}',
`NOMBRE ALIADO` = '{$AI}',
CANTIDAD_REAGENDAMIENTOS = '{$N}',
QTY_DECO_SD = '{$AK}',
QTY_DECO_HD = '{$AL}',
QTY_DECO_PVR = '{$AM}',
QTY_DECO_TOTAL = '{$AN}',
FECHA_AGENDA =
CASE WHEN `ESTADO ORDEN` = 'PROGRAMACION'
THEN '{$L}' ELSE FECHA_AGENDA END";
          if($con->query($sql)){
              return "Ok";
          }else{
              return $con->error;
							// return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function cargaTempDTH($fecha_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_DTH
(TIPO, DISTRIBUIDOR, VENDEDOR, TKFECHA, TKHORA, TIEMPO, DH, NATENCION, DECOS, PLANDESCRIPCION, COSTO_INSTALACION, FECHA_AGENDA, HORA_AGENDA, CANTIDAD_REAGENDAMIENTOS, RUT, CLIENTE, REGION, COMUNA, CALLE, NUMERO, DEPTO, FONO1, FONO2, REFERENCIA, TIPO_CALLE, TIPO_TICKET, NOMBRE_ZONA, OBSERVACIONES, `kit tarjetas`, `kit lnb`, `kit splitter`, `kit antena`, `kit cable`, `kit brazo telescopico`, `NOMBRE ALIADO`, `ESTADO ORDEN`, fecha_carga, hora_carga)
SELECT TIPO, DISTRIBUIDOR, VENDEDOR, TKFECHA, TKHORA, TIEMPO, DH, NATENCION, DECOS, PLANDESCRIPCION, COSTO_INSTALACION, FECHA_AGENDA, HORA_AGENDA, CANTIDAD_REAGENDAMIENTOS, RUT, CLIENTE, REGION, COMUNA, CALLE, NUMERO, DEPTO, FONO1, FONO2, REFERENCIA, TIPO_CALLE, TIPO_TICKET, NOMBRE_ZONA, OBSERVACIONES, `kit tarjetas`, `kit lnb`, `kit splitter`, `kit antena`, `kit cable`, `kit brazo telescopico`, `NOMBRE ALIADO`, `ESTADO ORDEN`, fecha_carga, hora_carga
FROM SIAD_NOTIF_DTH_TEMP
WHERE fecha_carga = '{$fecha_carga}'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error;
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function insertDatosClaroHFC($A,$B,$C,$D,$E,$F,$G,$H,$I,$J,$K,$L,$M,$N,$O,$P,$Q,$R,$S,$T,$U,$V,$X,$Y,$Z,$AA,$AB,$AC,$AD,$AE,$AF,$AG,$AH,$AI,$AJ,$AK,$AL,$AM,$AN,$AO,$AP,$AQ,$AR,$AS,$AT,$AU,$AV,$AW,$AX,$AY,$AZ,$fecha_carga,$hora_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_HFC
(`Fecha Primer Agendamiento`, `Hora Primer Agendamiento`, `Fecha Ag`, `Hora Ag`, Folio, Estado, `Fecha de Creacion Orden`, `Tipo Cliente`, `Tipo de Trabajo`, Rut, `Nombre del Clíente`, `Oferta comercial anterior`, `Oferta comercial anterior Deco HD`, `Oferta comercial anterior Deco`, `Oferta comercial anterior Telefonia`, `Oferta comercial anterior Internet`, `Oferta comercial anterior adicionales`, `Oferta comercial actual`, `Oferta comercial actual Deco HD`, `Oferta comercial actual Deco`, `Oferta comercial actual Telefonia`, `Oferta comercial actual Internet`, `Oferta comercial actual adicionales`, Domicilio, `Nodo Optico`, Servicio, `Número de Reagendamiento`, `Grupo Técnicos`, `Nombre Aliado`, `Tipo de Venta`, `ID vendedor`, `Nombre vendedor`, `Fecha de Ingreso a Despacho`, `Fecha Actualizacion/Cierre`, `Id Técnico`, Técnico, Comuna, `Usuario Creador de la orden`, `Resolucion de Reparacion`, Fono, Comentarios, `MARCA CROSS`, `AGRUPA ACTIVIDAD`, `OBS PORTABILIDAD`, `DIAS INGRESO`, `DIAS AGENDA`, `ESTADO ORDEN`, `CUENTA NETO PNDTE`, TECNOLOGIA, ZONA, REGION, fecha_carga, hora_carga)
VALUES('{$A}','{$B}','{$C}','{$D}','{$E}','{$F}','{$G}','{$H}','{$I}','{$J}','{$K}','{$L}','{$M}','{$N}','{$O}','{$P}','{$Q}','{$R}','{$S}','{$T}','{$U}','{$V}','{$X}','{$Y}','{$Z}','{$AA}','{$AB}','{$AC}','{$AD}','{$AE}','{$AF}','{$AG}','{$AH}','{$AI}','{$AJ}','{$AK}','{$AL}','{$AM}','{$AN}','{$AO}','{$AP}','{$AQ}','{$AR}','{$AS}','{$AT}','{$AU}','{$AV}','{$AW}','{$AX}','{$AY}','{$AZ}','{$fecha_carga}','{$hora_carga}')
ON DUPLICATE KEY UPDATE
`ESTADO ORDEN` = '{$AV}',
`Número de Reagendamiento` = '{$AB}'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error;
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function cargaTempHFC($fecha_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_HFC
(`Fecha Primer Agendamiento`, `Hora Primer Agendamiento`, `Fecha Ag`, `Hora Ag`, Folio, Estado, `Fecha de Creacion Orden`, `Tipo Cliente`, `Tipo de Trabajo`, Rut, `Nombre del Clíente`, `Oferta comercial anterior`, `Oferta comercial anterior Deco HD`, `Oferta comercial anterior Deco`, `Oferta comercial anterior Telefonia`, `Oferta comercial anterior Internet`, `Oferta comercial anterior adicionales`, `Oferta comercial actual`, `Oferta comercial actual Deco HD`, `Oferta comercial actual Deco`, `Oferta comercial actual Telefonia`, `Oferta comercial actual Internet`, `Oferta comercial actual adicionales`, Domicilio, `Nodo Optico`, Servicio, `Número de Reagendamiento`, `Grupo Técnicos`, `Nombre Aliado`, `Tipo de Venta`, `ID vendedor`, `Nombre vendedor`, `Fecha de Ingreso a Despacho`, `Fecha Actualizacion/Cierre`, `Id Técnico`, Técnico, Comuna, `Usuario Creador de la orden`, `Resolucion de Reparacion`, Fono, Comentarios, `MARCA CROSS`, `AGRUPA ACTIVIDAD`, `OBS PORTABILIDAD`, `DIAS INGRESO`, `DIAS AGENDA`, `ESTADO ORDEN`, `CUENTA NETO PNDTE`, TECNOLOGIA, ZONA, REGION, fecha_carga, hora_carga)
SELECT `Fecha Primer Agendamiento`, `Hora Primer Agendamiento`, `Fecha Ag`, `Hora Ag`, Folio, Estado, `Fecha de Creacion Orden`, `Tipo Cliente`, `Tipo de Trabajo`, Rut, `Nombre del Clíente`, `Oferta comercial anterior`, `Oferta comercial anterior Deco HD`, `Oferta comercial anterior Deco`, `Oferta comercial anterior Telefonia`, `Oferta comercial anterior Internet`, `Oferta comercial anterior adicionales`, `Oferta comercial actual`, `Oferta comercial actual Deco HD`, `Oferta comercial actual Deco`, `Oferta comercial actual Telefonia`, `Oferta comercial actual Internet`, `Oferta comercial actual adicionales`, Domicilio, `Nodo Optico`, Servicio, `Número de Reagendamiento`, `Grupo Técnicos`, `Nombre Aliado`, `Tipo de Venta`, `ID vendedor`, `Nombre vendedor`, `Fecha de Ingreso a Despacho`, `Fecha Actualizacion/Cierre`, `Id Técnico`, Técnico, Comuna, `Usuario Creador de la orden`, `Resolucion de Reparacion`, Fono, Comentarios, `MARCA CROSS`, `AGRUPA ACTIVIDAD`, `OBS PORTABILIDAD`, `DIAS INGRESO`, `DIAS AGENDA`, `ESTADO ORDEN`, `CUENTA NETO PNDTE`, TECNOLOGIA, ZONA, REGION, fecha_carga, hora_carga
FROM SIAD_NOTIF_HFC_TEMP
WHERE fecha_carga = '{$fecha_carga}'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error;
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function insertDatosClaroPyme($A,$B,$C,$D,$E,$F,$G,$H,$I,$J,$K,$L,$M,$N,$O,$P,$fecha_carga,$hora_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_PYME
				(FOLIO, COMUNA, `TIPO TRABAJO`, `NOMBRE ACTIVIDAD`, `GRUPO TECNICO`, ALIADO, `OFERTA COMERCIAL ANTERIOR`, `OFERTA COMERCIAL ACTUAL`, ESTADO, `FECHA PRIMERA AGENDA`, `FECHA AGENDA`, `FECHA INGRESO DESPACHO`, `FECHA ACTULIZACION CIERRE`, COMENTARIOS, ZONA, REGION, fecha_carga, hora_carga)
				VALUES('{$A}','{$B}','{$C}','{$D}','{$E}','{$F}','{$G}','{$H}','{$I}','{$J}','{$K}','{$L}','{$M}','{$N}','{$O}','{$P}','{$fecha_carga}','{$hora_carga}')";
          if($con->query($sql)){
              return "Ok";
          }else{
              return $con->error;
							// return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function cargaTempPyme($fecha_carga){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "INSERT INTO SIAD_NOTIF_PYME (FOLIO, COMUNA, `TIPO TRABAJO`, `NOMBRE ACTIVIDAD`, `GRUPO TECNICO`, ALIADO, `OFERTA COMERCIAL ANTERIOR`, `OFERTA COMERCIAL ACTUAL`, ESTADO, `FECHA PRIMERA AGENDA`, `FECHA AGENDA`, `FECHA INGRESO DESPACHO`, `FECHA ACTULIZACION CIERRE`, COMENTARIOS, ZONA, REGION, fecha_carga, hora_carga)
				SELECT FOLIO, COMUNA, `TIPO TRABAJO`, `NOMBRE ACTIVIDAD`, `GRUPO TECNICO`, ALIADO, `OFERTA COMERCIAL ANTERIOR`, `OFERTA COMERCIAL ACTUAL`, ESTADO, `FECHA PRIMERA AGENDA`, `FECHA AGENDA`, `FECHA INGRESO DESPACHO`, `FECHA ACTULIZACION CIERRE`, COMENTARIOS, ZONA, REGION, fecha_carga, hora_carga
				FROM SIAD_NOTIF_PYME_TEMP
				WHERE fecha_carga = '{$fecha_carga}'";
          if($con->query($sql)){
              return "Ok";
          }else{
              return $con->error;
							// return "Error2";
							return $sql;
          }
      }else{
          return "Error";
      }
  }

	function truncateOTTempDTH(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "TRUNCATE TABLE SIAD_NOTIF_DTH_TEMP";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							// return $sql;
							return "Error2";
          }
      }else{
          return "Error";
      }
  }

	function truncateOTTempHFC(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "TRUNCATE TABLE SIAD_NOTIF_HFC_TEMP";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function truncateOTTempPYME(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "TRUNCATE TABLE SIAD_NOTIF_PYME_TEMP";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function normalizaDTH(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "UPDATE SIAD_NOTIF_DTH
SET `ESTADO ORDEN` = REPLACE(`ESTADO ORDEN`, '
                        </table></html>1','')
WHERE `ESTADO ORDEN` LIKE '%
                        </table></html>1%'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function normalizaDTHProgramacion(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "UPDATE SIAD_NOTIF_DTH
SET ESTADO_DESPACHO = 'Programacion'
WHERE `ESTADO ORDEN` = 'PROGRAMACION'
AND ESTADO_DESPACHO <> 'Programacion'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function normalizaDTHAgendamiento(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "UPDATE SIAD_NOTIF_DTH
SET ESTADO_DESPACHO = 'Confirmado'
WHERE `ESTADO ORDEN` <> 'PROGRAMACION'
AND ESTADO_DESPACHO = 'Programacion'";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }

	function cargaOTDTH(){
      $con = conectar();
      if($con != 'No conectado'){
          $sql = "CALL CARGA_OT_DTH(DATE_FORMAT(NOW(), '%Y-%m-%d'))";
          if($con->query($sql)){
              return "Ok";
          }else{
              // return $con->error . "<br><br>";
							return "Error2";
							// return $sql;
          }
      }else{
          return "Error";
      }
  }
?>
